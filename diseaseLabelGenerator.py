from pymongo import MongoClient
import re

## init 
client = MongoClient()
db = client['arraymap']
samples = db['samples_copy']


## disease keywords 
diseases = {'fanconi': ['DOID:13636','Fanconi anemia'],
			'down': ['DOID:14250', 'Down syndrome'],
			'ollier': ['DOID:4624', 'Ollier disease'],
			'fraumeni': ['DOID:3012', 'Li-Fraumeni syndrome'],
			'mafucci': ['DOID:0060221', 'Maffuccis syndrome'],
			'richter': ['DOID:1703', 'Richter’s syndrome'],
			'lynch': ['DOID:3883', 'Lynch syndrome'],
			'fap': ['DOID:0050424', 'Familial Adenomatous Polyposis'],
			'neurofibromatosis': ['DOID:8712', 'Neurofibromatosis']}


## update the database
for k,v in diseases.items():
	keyword = k
	ontology_term = v[0]
	disease_name = v[1] 
	samples.update_many({'DIAGNOSISTEXT':{'$regex': re.compile(keyword, re.IGNORECASE) }}, 
		{'$set': {'DISEASE':[{'ONTOLOGY':ontology_term, 'PHENOTYPE':disease_name}]}})






#print(samples.find({'DIAGNOSISTEXT':{'$regex': re.compile('lynch', re.IGNORECASE)}}).count())



