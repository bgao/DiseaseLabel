This script search through the DIAGNOSISTEXT in "samples" and looks for disease keywords.
<br>
It will generate a new DISEASE attribute if a disease name is found.
<br>

list of current keywords:

* Fanconi anemia
* Down syndrome
* Ollier syndrome
* Li-Fraumeni syndrome
* Mafucci syndrome
* Richter-syndrome
* Lynch syndrome
* FAP
* Neurofibromatosis type 1

Note:
* keyword 'MAP' does not have any DOID term, so it is not used.
* by default, the script operates on a clone of the collection "sampels", called "samples_copy".